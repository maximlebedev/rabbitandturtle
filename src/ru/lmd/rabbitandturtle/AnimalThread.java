package ru.lmd.rabbitandturtle;

public class AnimalThread extends Thread {

    private String name;

    AnimalThread(String name, int priority) {
        this.name = name;
        setName(name);
        setPriority(priority);
    }

    public void run() {
        int i;
        for (i = 0; i <= 500; i += 50) {
            if ((i == 250) && (name.equals("rabbit"))) {
                setPriority(1);
                System.out.println("Установка приоритета " + getName() + " на: " + getPriority());
            }
            if ((i == 250) && (name.equals("turtle"))) {
                setPriority(10);
                System.out.println("Установка приоритета " + getName() + " на: " + getPriority());
            }
            if (i == 500) {
                System.out.println(getName() + ": ///////// FINISH /////////");
            }
            System.out.println(getName() + ": " + i + " m.");
        }
    }
}